<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Hb Airways</title>
<link href="<c:url value="/bootstrap/css/my_style.css"/>"
	rel="stylesheet">
<link href="<c:url value="/bootstrap/css/bootstrap.min.css"/>"
	rel="stylesheet">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">HB Airways</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link" href="#">Home
						<span class="sr-only">(current)</span>
				</a></li>
				<li class="nav-item"><a class="nav-link" href="#">Destination</a>
				</li>
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> Countries </a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="#">Europe</a> <a
							class="dropdown-item" href="#">North America</a> <a
							class="dropdown-item" href="#">Sud America</a> <a
							class="dropdown-item" href="#">Asia</a>
					</div></li>
			</ul>
			<form class="form-inline my-2 my-lg-0">
				<a class="btn btn-primary" href="connexion" role="buttonConnexion">Connexion</a>
				<a class="btn btn-primary" href="inscription" role="buttonInscription">Inscription</a>
<!-- 				<input class="form-control mr-sm-2" type="search" -->
<!-- 					placeholder="Search" aria-label="Search"> -->
<!-- 				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> -->
			</form>
		</div>
	</nav>
<h1>Human Booster Airways !</h1>
<h2><marquee>Bienvenue sur HumanBooster Airways :)</marquee></h2>
<h2>Connexion</h2>
	<form action="connexion" method="post">
	
	<label>Pseudo</label> <input type="text" name="PSEUDO" placeHolder="Votre pseudo"><br><br>
 
    <label>Mot de Passe</label> <input type="password" name="MOT_DE_PASSE" placeHolder="Votre mot de passe"><br><br>
	
	<input type="submit" value="Connexion">
	</form>
	<br>
	<br>
	<a href="inscription">Inscription</a> <br>
	
</body>
</body>
</html>