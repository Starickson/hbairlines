<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Réservation</title>
<link href="style/theme1.css" rel="stylesheet">
</head>
<body>
<h1>Nouvelle Réservation</h1>
<form:form modelAttribute="reservation" action="reservationModelAttribute" method="post">
<label>Vol</label>De ${reservation.vol.aerodromeDepart.nom} à ${reservation.vol.aerodromeArrivee.nom}<br>
<label>&nbsp;</label>Départ le ${reservation.vol.dateHeureDepartFormatee}<br>
<label>&nbsp;</label>Arrivée le ${reservation.vol.dateHeureArriveeFormatee}<br>
<form:hidden path="vol.id"/>
<br>
<label>Avion</label>${reservation.vol.avion.immatriculation} de type ${reservation.vol.avion.typeAppareil.nom}<br>
<br>
<label>Prix</label>${reservation.vol.prixEnEuros} €<br>

<br>
<label>Passager</label><form:select path="passager">
<form:options items="${passagers}" itemLabel="prenomEtNom" itemValue="id"/>
</form:select>
<form:errors path="passager" cssClass="error"/>
<br><br>
<label>Service(s) souhaité(s)</label><form:select path="services" size="6">
<form:options items="${services}" itemLabel="nomEtPrixEnEuros" itemValue="id"/>
</form:select>
<br><br>
<label>Numéro de carte</label><form:input path="numeroCarte"/>
<form:errors path="numeroCarte" cssClass="error"/>
<br>
<label>Mois expiration</label><form:input path="moisExpiration"/>
<form:errors path="moisExpiration" cssClass="error"/>
<br>
<label>Année expiration</label><form:input path="anneeExpiration"/>
<form:errors path="anneeExpiration" cssClass="error"/>
<br>
<label>Cryptogramme</label><form:input path="cryptogramme"/>
<form:errors path="cryptogramme" cssClass="error"/>
<br><br>
<label>&nbsp;</label><form:button >Réserver</form:button>
</form:form>
</body>
</html>