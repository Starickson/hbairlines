<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Flotte</title>
</head>
<body>
<h1>Flotte</h1>
Voici la liste des avions de la compagnie:
<ol>
<c:forEach items="${avions}" var="avion">
<li>${avion.immatriculation} ${avion.typeAppareil.nom} - ${avion.datePremierVol}</li>
</c:forEach>
</ol>
</body>
</html>