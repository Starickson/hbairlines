package fr.humanbooster.fx.belair.business;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Service {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	private String nom;
	
	private float prixEnEuros;
	
	
	@ManyToMany()
	private List<Reservation> reservations;
	
	public Service() {
		// TODO Auto-generated constructor stub
	}

	public Service(String nom) {
		super();
		this.nom = nom;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public float getPrixEnEuros() {
		return prixEnEuros;
	}

	public String getNomEtPrixEnEuros() {
		return nom + " (" + prixEnEuros + " €)";
	}

	public void setPrixEnEuros(float prixEnEuros) {
		this.prixEnEuros = prixEnEuros;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	@Override
	public String toString() {
		return "Service [id=" + id + ", nom=" + nom + ", prixEnEuros=" + prixEnEuros + "]";
	}
	
	
}
