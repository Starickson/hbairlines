package fr.humanbooster.fx.belair.business;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


@Entity
public class Vol {

	private static final String FORMAT_DATE_HEURE = "dd/MM/yyyy HH:mm";
	private static SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATE_HEURE);

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	
	@ManyToOne
	private Aeroport aeroportDepart;

	
	@ManyToOne
	private Aeroport aeroportArrivee;

	private Date dateHeureDepart;
	
	private Date dateHeureArrivee;

	@ManyToOne
	private Avion avion;

	@OneToMany(mappedBy = "vol", fetch=FetchType.EAGER)
	private List<Reservation> reservations;

	private Float prixEnEuros;

	public Vol() {
		// TODO Auto-generated constructor stub
	}

	public Vol(Aeroport aeroportDepart, Aeroport aeroportArrivee, Date dateHeureDepart, Date dateHeureArrivee,
			Avion avion, Float prixEnEuros) {
		super();
		this.aeroportDepart = aeroportDepart;
		this.aeroportArrivee = aeroportArrivee;
		this.dateHeureDepart = dateHeureDepart;
		this.dateHeureArrivee = dateHeureArrivee;
		this.avion = avion;
		this.prixEnEuros = prixEnEuros;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public Aeroport getAeroportDepart() {
		return aeroportDepart;
	}

	public void setAeroportDepart(Aeroport aeroportDepart) {
		this.aeroportDepart = aeroportDepart;
	}

	public Aeroport getAeroportArrivee() {
		return aeroportArrivee;
	}

	public void setAeroportArrivee(Aeroport aeroportArrivee) {
		this.aeroportArrivee = aeroportArrivee;
	}

	public Date getDateHeureDepart() {
		return dateHeureDepart;
	}

	public String getDateHeureDepartFormatee() {
		return sdf.format(getDateHeureDepart());
	}
	
	public void setDateHeureDepart(Date dateHeureDepart) {
		this.dateHeureDepart = dateHeureDepart;
	}

	public Date getDateHeureArrivee() {
		return dateHeureArrivee;
	}

	public String getDateHeureArriveeFormatee() {
		return sdf.format(getDateHeureArrivee());
	}

	public void setDateHeureArrivee(Date dateHeureArrivee) {
		this.dateHeureArrivee = dateHeureArrivee;
	}

	public Avion getAvion() {
		return avion;
	}

	public void setAvion(Avion avion) {
		this.avion = avion;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	public Float getPrixEnEuros() {
		return prixEnEuros;
	}

	public void setPrixEnEuros(Float prixEnEuros) {
		this.prixEnEuros = prixEnEuros;
	}

	@Override
	public String toString() {
		return "Vol [id=" + id + ", aeroportDepart=" + aeroportDepart + ", aeroportArrivee=" + aeroportArrivee
				+ ", dateHeureDepart=" + dateHeureDepart + ", dateHeureArrivee=" + dateHeureArrivee + ", avion=" + avion.getImmatriculation()
				+ ", reservations=" + reservations + ", prixEnEuros=" + prixEnEuros + "]";
	}
	
	
}
