package fr.humanbooster.fx.belair.business;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


@Entity
public class Duree {

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;


	@ManyToOne
	private Aeroport aeroportDepart;

	@ManyToOne
	private Aeroport aeroportArrivee;
	
	private int dureeDuVolEnMinutes;
	
	public Duree() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Aeroport getAeroportDepart() {
		return aeroportDepart;
	}

	public void setAeroportDepart(Aeroport aeroportDepart) {
		this.aeroportDepart = aeroportDepart;
	}

	public Aeroport getAeroportArrivee() {
		return aeroportArrivee;
	}

	public void setAeroportArrivee(Aeroport aeroportArrivee) {
		this.aeroportArrivee = aeroportArrivee;
	}

	public int getDureeDuVolEnMinutes() {
		return dureeDuVolEnMinutes;
	}

	public void setDureeDuVolEnMinutes(int dureeDuVolEnMinutes) {
		this.dureeDuVolEnMinutes = dureeDuVolEnMinutes;
	}
	
}
