package fr.humanbooster.fx.belair.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.CreditCardNumber;
import org.hibernate.validator.constraints.Range;

@Entity
public class Reservation {

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	
	@ManyToOne
	@NotNull(message="merci de choisir un vol")
	private Vol vol;
	
	
	@ManyToOne
	@NotNull(message="Merci de choisir un passager")
	private Passager passager;

	private Date dateHeureReservation;

	@NotNull(message="Merci de préciser un numero de carte")
	@CreditCardNumber(message="Le numéro de carte n''est pas valide")
	private String numeroCarte;

	private int moisExpiration;
		
	private int anneeExpiration;
	
	@NotNull
	@Range(min=100, max=999, message="Le cryptogramme doit être compris entre 100 et 999")
	private String cryptogramme;
	
	
	@ManyToMany(mappedBy = "reservations",fetch=FetchType.EAGER)
	private List<Service> services;
	
	public Reservation() {
		services = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Vol getVol() {
		return vol;
	}

	public void setVol(Vol vol) {
		this.vol = vol;
	}

	public Passager getPassager() {
		return passager;
	}

	public void setPassager(Passager passager) {
		this.passager = passager;
	}

	public Date getDateHeureReservation() {
		return dateHeureReservation;
	}

	public void setDateHeureReservation(Date dateHeureReservation) {
		this.dateHeureReservation = dateHeureReservation;
	}

	public String getNumeroCarte() {
		return numeroCarte;
	}

	public void setNumeroCarte(String numeroCarte) {
		this.numeroCarte = numeroCarte;
	}

	public int getMoisExpiration() {
		return moisExpiration;
	}

	public void setMoisExpiration(int moisExpiration) {
		this.moisExpiration = moisExpiration;
	}

	public int getAnneeExpiration() {
		return anneeExpiration;
	}

	public void setAnneeExpiration(int anneeExpiration) {
		this.anneeExpiration = anneeExpiration;
	}

	public String getCryptogramme() {
		return cryptogramme;
	}

	public void setCryptogramme(String cryptogramme) {
		this.cryptogramme = cryptogramme;
	}

	public List<Service> getServices() {
		return services;
	}

	public void setServices(List<Service> services) {
		this.services = services;
	}

	@Override
	public String toString() {
		return "Reservation [id=" + id + ", vol=" + vol.getId() + ", passager=" + passager + ", dateHeureReservation="
				+ dateHeureReservation + ", numeroCarte=" + numeroCarte + ", moisExpiration=" + moisExpiration
				+ ", anneeExpiration=" + anneeExpiration + ", cryptogramme=" + cryptogramme + services +  "]";
	}
	
}