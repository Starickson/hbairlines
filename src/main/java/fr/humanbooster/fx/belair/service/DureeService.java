package fr.humanbooster.fx.belair.service;

import java.util.List;

import fr.humanbooster.fx.belair.business.Aeroport;
import fr.humanbooster.fx.belair.business.Duree;

public interface DureeService {

	public Duree ajouterDuree(Duree duree);
	
	public Duree ajouterDuree(int dureeDuVolEnMinutes, Aeroport aeroportDepart, Aeroport aeroportArrivee);
	
	public Duree recupererDuree(Long id);

	public List<Duree> recupererDurees();
}
