package fr.humanbooster.fx.belair.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.humanbooster.fx.belair.business.Avion;
import fr.humanbooster.fx.belair.business.TypeAppareil;
import fr.humanbooster.fx.belair.dao.AvionDao;
import fr.humanbooster.fx.belair.service.AvionService;

@Service
public class AvionServiceImpl implements AvionService {

	private AvionDao avionDao;
	
	
	public AvionServiceImpl(AvionDao avionDao) {
		super();
		this.avionDao = avionDao;
	}

	@Override
	public Avion ajouterAvion(Avion avion) {
		return avionDao.save(avion);
	}

	@Override
	public Avion ajouterAvion(String immatriculation, Date datePremierVol, TypeAppareil typeAppareil) {
		Avion avion = new Avion();
		avion.setImmatriculation(immatriculation);
		avion.setDatePremierVol(datePremierVol);
		avion.setTypeAppareil(typeAppareil);
		return avionDao.save(avion);
	}

	@Override
	public Avion recupererAvion(Long id) {
		return avionDao.findOne(id);
	}

	@Override
	public List<Avion> recupererAvions() {
		return avionDao.findAll();
	}

	@Override
	public Page<Avion> recupererAvions(Pageable pageable) {
		return avionDao.findAll(pageable);
	}

}
