package fr.humanbooster.fx.belair.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import fr.humanbooster.fx.belair.business.Aeroport;
import fr.humanbooster.fx.belair.business.Avion;
import fr.humanbooster.fx.belair.business.Vol;

public interface VolService {

	public Vol ajouterVol(Vol vol);

	public Vol ajouterVol(Date dateHeureDepart, Date dateHeureArrivee, float prixEnEuros, Aeroport aeroportDepart,
			Aeroport aeroportArrivee, Avion avion);

	public Vol recupererVol(Long id);

	public List<Vol> recupererVols();
	
	public Page<Vol> recupererVols(Pageable pageable);
}
