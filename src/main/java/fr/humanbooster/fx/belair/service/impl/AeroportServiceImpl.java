package fr.humanbooster.fx.belair.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.humanbooster.fx.belair.business.Aeroport;
import fr.humanbooster.fx.belair.dao.AeroportDao;
import fr.humanbooster.fx.belair.service.AeroportService;

// Les stereotypes de Spring sont les suivants: 
// Repository, Service, Controller et Component
// Ne pas oubliez cette annotation au dessus de vos classes de Service !
@Service
public class AeroportServiceImpl implements AeroportService {

	// On déclare un objet de type DAO
	// On demande à Spring de créer l'objet DAO
	// Principe Hollywood: "ne nous appelez pas, nous vous appelerons"
	@Autowired
	private AeroportDao aeroportDao;
	
	@Override
	public Aeroport ajouterAeroport(String nom) {
				
		// On déclare un objet métier
		Aeroport aeroport = new Aeroport(nom);
		// On confie l'objet métier à la DAO
		// On réaffecte l'objet métier car la DAO attribue un id 
		// à l'aérodrome
		aeroport = aeroportDao.save(aeroport);
		// On renvoie l'objet métier
		return aeroport;
	}

	@Override
	public Aeroport recupererAeroport(Long id) {
		return aeroportDao.findOne(id);
	}

	@Override
	public List<Aeroport> recupererAeroports() {
		return aeroportDao.findAll();
	}

	@Override
	public Aeroport recupererAeroportParNom(String nom) {
		return aeroportDao.findAeroportByNom(nom);
	}

	@Override
	public long recupererNbAeroports() {
		return aeroportDao.count();
	}

}