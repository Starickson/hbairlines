package fr.humanbooster.fx.belair.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.humanbooster.fx.belair.business.Aeroport;
import fr.humanbooster.fx.belair.business.Duree;
import fr.humanbooster.fx.belair.dao.DureeDao;
import fr.humanbooster.fx.belair.service.DureeService;

@Service
public class DureeServiceImpl implements DureeService {

	private DureeDao dureeDao;
	
	
	
	public DureeServiceImpl(DureeDao dureeDao) {
		super();
		this.dureeDao = dureeDao;
	}

	@Override
	public Duree ajouterDuree(Duree duree) {
		return dureeDao.save(duree);
	}

	@Override
	public Duree ajouterDuree(int dureeDuVolEnMinutes, Aeroport aeroportDepart, Aeroport aeroportArrivee) {
		Duree duree = new Duree();
		duree.setDureeDuVolEnMinutes(dureeDuVolEnMinutes);
		duree.setAeroportDepart(aeroportDepart);
		duree.setAeroportArrivee(aeroportArrivee);
		return dureeDao.save(duree);
	}
	
	@Override
	public Duree recupererDuree(Long id) {
		return dureeDao.findOne(id);
	}

	@Override
	public List<Duree> recupererDurees() {
		return dureeDao.findAll();
	}

}
