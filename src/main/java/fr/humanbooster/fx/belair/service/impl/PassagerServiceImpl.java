package fr.humanbooster.fx.belair.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.humanbooster.fx.belair.business.Passager;
import fr.humanbooster.fx.belair.dao.PassagerDao;
import fr.humanbooster.fx.belair.service.PassagerService;

@Service
public class PassagerServiceImpl implements PassagerService {

	private PassagerDao passagerDao;

	
	public PassagerServiceImpl(PassagerDao passagerDao) {
		super();
		this.passagerDao = passagerDao;
	}

	@Override
	public Passager ajouterPassager(Passager passager) {
		return passagerDao.save(passager);
	}

	@Override
	public Passager ajouterPassager(String nom, String prenom, String email, String motDePasse) {
		Passager passager = new Passager();
		passager.setNom(nom);
		passager.setPrenom(prenom);
		passager.setEmail(email);
		passager.setMotDePasse(motDePasse);
		return passagerDao.save(passager);
	}

	@Override
	public Passager recupererPassager(Long id) {
		return passagerDao.findOne(id);
	}

	@Override
	public List<Passager> recupererPassagers() {
		return passagerDao.findAll();
	}

}
