package fr.humanbooster.fx.belair.service;

import java.util.List;

import fr.humanbooster.fx.belair.business.TypeAppareil;

public interface TypeAppareilService {

public TypeAppareil ajouterTypeAppareil(TypeAppareil typeAppareil);
	
	public TypeAppareil ajouterTypeAppareil(String nom, int nbSieges);
	
	public TypeAppareil recupererTypeAppareil(Long id);

	public List<TypeAppareil> recupererTypeAppareils();
}
