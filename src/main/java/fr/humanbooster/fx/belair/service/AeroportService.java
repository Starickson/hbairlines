package fr.humanbooster.fx.belair.service;

import java.util.List;

import fr.humanbooster.fx.belair.business.Aeroport;

public interface AeroportService {

	Aeroport ajouterAeroport(String nom);

	// Je fais la promesse qu'un jour un stagiaire HB du groupe 14
	// écrira cette méthode
	Aeroport recupererAeroport(Long id);

	/**
	 * Cette méthode renvoie tous les aérodromes stockés en base
	 * 
	 * @return une liste d'aérodromes
	 */
	List<Aeroport> recupererAeroports();

	Aeroport recupererAeroportParNom(String nom);
	
	long recupererNbAeroports();
}
