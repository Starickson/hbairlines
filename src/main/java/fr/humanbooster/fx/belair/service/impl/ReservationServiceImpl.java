package fr.humanbooster.fx.belair.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import fr.humanbooster.fx.belair.business.Passager;
import fr.humanbooster.fx.belair.business.Reservation;
import fr.humanbooster.fx.belair.business.Vol;
import fr.humanbooster.fx.belair.dao.ReservationDao;
import fr.humanbooster.fx.belair.service.ReservationService;

@Service
public class ReservationServiceImpl implements ReservationService {

	private ReservationDao reservationDao;
	
	
	public ReservationServiceImpl(ReservationDao reservationDao) {
		super();
		this.reservationDao = reservationDao;
	}

	@Override
	public Reservation ajouterReservation(Reservation reservation) {
		return reservationDao.save(reservation);
	}

	@Override
	public Reservation ajouterReservation(Date dateHeureReservation, String numeroCarte, int moisExpiration,
			int anneeExpiration, String cryptogramme, Passager passager, Vol vol) {
		Reservation reservation = new Reservation();
		reservation.setDateHeureReservation(dateHeureReservation);
		reservation.setNumeroCarte(numeroCarte);
		reservation.setMoisExpiration(moisExpiration);
		reservation.setAnneeExpiration(anneeExpiration);
		reservation.setCryptogramme(cryptogramme);
		reservation.setPassager(passager);
		reservation.setVol(vol);
		return reservationDao.save(reservation);
	}

	@Override
	public Reservation recupererReservation(Long id) {
		return reservationDao.findOne(id);
	}

	@Override
	public List<Reservation> recupererReservations() {
		return reservationDao.findAll();
	}

}
