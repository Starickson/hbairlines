package fr.humanbooster.fx.belair.service.impl;

import java.util.List;
import fr.humanbooster.fx.belair.business.Service;
import fr.humanbooster.fx.belair.dao.ServiceDao;
import fr.humanbooster.fx.belair.service.ServiceService;

@org.springframework.stereotype.Service
public class ServiceServiceImpl implements ServiceService {

	private ServiceDao serviceDao;
	
	public ServiceServiceImpl(ServiceDao serviceDao) {
		super();
		this.serviceDao = serviceDao;
	}

	@Override
	public Service ajouterService(fr.humanbooster.fx.belair.business.Service service) {
		return serviceDao.save(service);
	}

	@Override
	public Service ajouterService(String nom, float prixEnEuros) {
		fr.humanbooster.fx.belair.business.Service service = new fr.humanbooster.fx.belair.business.Service();
		service.setNom(nom);
		service.setPrixEnEuros(prixEnEuros);		
		return serviceDao.save(service);
	}

	@Override
	public Service recupererService(Long id) {
		return serviceDao.findOne(id);
	}

	@Override
	public List<Service> recupererServices() {
		return serviceDao.findAll();
	}

}