package fr.humanbooster.fx.belair.service;

import java.util.Date;
import java.util.List;

import fr.humanbooster.fx.belair.business.Passager;
import fr.humanbooster.fx.belair.business.Reservation;
import fr.humanbooster.fx.belair.business.Vol;

public interface ReservationService {

	public Reservation ajouterReservation(Reservation reservation);

	public Reservation ajouterReservation(Date dateHeureReservation, String numeroCarte, int moisExpiration,
			int anneeExpiration, String cryptogramme, Passager passager, Vol vol);

	public Reservation recupererReservation(Long id);

	public List<Reservation> recupererReservations();
}
