package fr.humanbooster.fx.belair.service;

import java.util.List;

import fr.humanbooster.fx.belair.business.Service;

public interface ServiceService {

	public Service ajouterService(Service service);
	
	public Service ajouterService(String nom, float prixEnEuros);
	
	public Service recupererService(Long id);

	public List<Service> recupererServices();
}
