package fr.humanbooster.fx.belair.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.humanbooster.fx.belair.business.Aeroport;
import fr.humanbooster.fx.belair.business.Avion;
import fr.humanbooster.fx.belair.business.Vol;
import fr.humanbooster.fx.belair.dao.VolDao;
import fr.humanbooster.fx.belair.service.VolService;

@Service
public class VolServiceImpl implements VolService {

	private VolDao volDao;
	
	public VolServiceImpl(VolDao volDao) {
		super();
		this.volDao = volDao;
	}

	@Override
	public Vol ajouterVol(Vol vol) {
		return volDao.save(vol);
	}

	@Override
	public Vol ajouterVol(Date dateHeureDepart, Date dateHeureArrivee, float prixEnEuros, Aeroport aeroportDepart,
			Aeroport aeroportArrivee, Avion avion) {
		Vol vol = new Vol();
		vol.setDateHeureDepart(dateHeureDepart);
		vol.setDateHeureArrivee(dateHeureArrivee);
		vol.setPrixEnEuros(prixEnEuros);
		vol.setAeroportDepart(aeroportDepart);
		vol.setAeroportArrivee(aeroportArrivee);
		vol.setAvion(avion);
		return volDao.save(vol);
	}

	@Override
	public Vol recupererVol(Long id) {
		return volDao.findOne(id);
	}

	@Override
	public List<Vol> recupererVols() {
		return volDao.findAll();
	}

	@Override
	public Page<Vol> recupererVols(Pageable pageable) {
		return volDao.findAll(pageable);
	}

}
