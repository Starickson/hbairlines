package fr.humanbooster.fx.belair.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.humanbooster.fx.belair.business.TypeAppareil;
import fr.humanbooster.fx.belair.dao.TypeAppareilDao;
import fr.humanbooster.fx.belair.service.TypeAppareilService;

@Service
public class TypeAppareilServiceImpl implements TypeAppareilService {

	private TypeAppareilDao typeAppareilDao;
	
	
	public TypeAppareilServiceImpl(TypeAppareilDao typeAppareilDao) {
		super();
		this.typeAppareilDao = typeAppareilDao;
	}

	@Override
	public TypeAppareil ajouterTypeAppareil(TypeAppareil typeAppareil) {
		return typeAppareilDao.save(typeAppareil);
	}

	@Override
	public TypeAppareil ajouterTypeAppareil(String nom, int nbSieges) {
		TypeAppareil typeAppareil = new TypeAppareil();
		typeAppareil.setNom(nom);
		typeAppareil.setNbSieges(nbSieges);
		return typeAppareilDao.save(typeAppareil);
	}

	@Override
	public TypeAppareil recupererTypeAppareil(Long id) {
		return typeAppareilDao.findOne(id);
	}

	@Override
	public List<TypeAppareil> recupererTypeAppareils() {
		return typeAppareilDao.findAll();
	}

}
