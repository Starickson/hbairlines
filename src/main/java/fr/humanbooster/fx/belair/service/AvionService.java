package fr.humanbooster.fx.belair.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import fr.humanbooster.fx.belair.business.Avion;
import fr.humanbooster.fx.belair.business.TypeAppareil;

public interface AvionService {

	public Avion ajouterAvion(Avion avion);
	
	public Avion ajouterAvion(String immatriculation, Date datePremierVol, TypeAppareil typeAppareil);
	
	public Avion recupererAvion(Long id);

	public List<Avion> recupererAvions();
	
	public Page<Avion> recupererAvions(Pageable pageable);
}
