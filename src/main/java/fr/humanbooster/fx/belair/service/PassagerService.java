package fr.humanbooster.fx.belair.service;

import java.util.List;

import fr.humanbooster.fx.belair.business.Passager;

public interface PassagerService {

	public Passager ajouterPassager(Passager passager);
	
	public Passager ajouterPassager(String nom, String prenom, String email, String motDePasse);
	
	public Passager recupererPassager(Long id);

	public List<Passager> recupererPassagers();

}
