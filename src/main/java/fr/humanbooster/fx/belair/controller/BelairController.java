package fr.humanbooster.fx.belair.controller;

import java.beans.PropertyEditorSupport;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.humanbooster.fx.belair.business.Aeroport;
import fr.humanbooster.fx.belair.business.Avion;
import fr.humanbooster.fx.belair.business.Passager;
import fr.humanbooster.fx.belair.business.Reservation;
import fr.humanbooster.fx.belair.business.Vol;
import fr.humanbooster.fx.belair.service.AeroportService;
import fr.humanbooster.fx.belair.service.AvionService;
import fr.humanbooster.fx.belair.service.DureeService;
import fr.humanbooster.fx.belair.service.PassagerService;
import fr.humanbooster.fx.belair.service.ReservationService;
import fr.humanbooster.fx.belair.service.ServiceService;
import fr.humanbooster.fx.belair.service.TypeAppareilService;
import fr.humanbooster.fx.belair.service.VolService;

@Controller
@RequestMapping("/")
public class BelairController {

	// Spring va créer l'objet aeroportService
	// Il trouvera la classe d'implémentation grâce à l'annotation @Service
	// sur AeroportServiceImpl
	private AeroportService aeroportService;
	private DureeService dureeService;
	private TypeAppareilService typeAppareilService;
	private AvionService avionService;
	private PassagerService passagerService;
	private ServiceService serviceService;
	private VolService volService;
	private ReservationService reservationService;

	public BelairController(AeroportService aeroportService, DureeService dureeService,
			TypeAppareilService typeAppareilService, AvionService avionService, PassagerService passagerService,
			ServiceService serviceService, VolService volService, ReservationService reservationService) {
		super();
		this.aeroportService = aeroportService;
		this.dureeService = dureeService;
		this.typeAppareilService = typeAppareilService;
		this.avionService = avionService;
		this.passagerService = passagerService;
		this.serviceService = serviceService;
		this.volService = volService;
		this.reservationService = reservationService;
	}
	
	@RequestMapping(value = { "/index", "/"})
	public ModelAndView accueil() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("index");
		return mav;
	}
	
	@GetMapping(value = { "/inscription" })
	public ModelAndView inscriptionGet() {
		ModelAndView mav = new ModelAndView();
		mav.addObject("passager", new Passager());
		mav.setViewName("inscription");
		return mav;
	}
	
	@GetMapping(value = { "/connexion" })
	public ModelAndView connexionGet() { 
		ModelAndView mav = new ModelAndView();
		mav.setViewName("connexion");
		return mav;
	}
	
	@PostMapping(value = {  "/inscriptionModelAttribute" })
	public ModelAndView inscriptionPost(@Valid @ModelAttribute Passager passager, BindingResult result) {
		// on demande à Spring si la validation de l'objet à generer a des erreurs
		if (result.hasErrors()) {
			ModelAndView mav = inscriptionGet();
			mav.addObject("passager", passager);
			return mav;
		} else {
			passagerService.ajouterPassager(passager);
			ModelAndView mav = new ModelAndView("connexion");
			mav.addObject("passager", passager);
			return mav;
		}
	}
	@RequestMapping(value = { "/listeDesVols"})
	public ModelAndView listeDesVolsGet() {
		// On déclare un objet ModelAndView et on l'instancie
		ModelAndView mav = new ModelAndView();
		// On définit la vue associée au mav
		// Ici la vue correspond au fichier index.jsp
		// NB: il n'est pas nécessaire de préciser l'extension .jsp
		mav.setViewName("listeDesVols");
		// On récupère tous les aérodromes et on les place dans le mav
		mav.addObject("aeroports", aeroportService.recupererAeroports());
		// On ajoute à la mav la liste des vols
		mav.addObject("vols", volService.recupererVols());
		
		return mav;
	}

	// Méthode du contrôleur qui redirige vers la page flotte.jsp
	// On doit préciser les URL que la méthode prend en charge
	// C'est l'équivalent de @WebServlet
	// Si un internaute se rend sur l'adresse http://localhost:82/flotte
	// la méthode flotteGet est invoquée
	@GetMapping({"/flotte", "flottes"})
	public ModelAndView flotteGet() {
		// On déclare et on instancie un objet ModelAndView
		// Dans le constructeur de ModelAndView on précise le nom de la vue
		ModelAndView mav = new ModelAndView("flotte");
		mav.addObject("avions", avionService.recupererAvions());
		return mav;
	}

	@GetMapping(value= {"/reservation"})
	public ModelAndView reservationGet(@RequestParam(name="ID_VOL") Long idVol) {
		ModelAndView mav = new ModelAndView("reservation");
		// On place dans l'objet mav le vol selectionnée
		mav.addObject("vol", volService.recupererVol(idVol));
		// On place dans l'objet mav tous les passagers
		mav.addObject("passagers", passagerService.recupererPassagers());
		// On place dans l'objet mav tous les services
		mav.addObject("services", serviceService.recupererServices());
		return mav;
	}
	
//	@GetMapping(value= {"/reservationModelAttribute"})
//	public ModelAndView reservationModelAttributeGet(@RequestParam(name="ID_VOL") Long idVol) {
//		// Le mav a pour vue: reservationModelAttribute.jsp
//		ModelAndView mav = new ModelAndView("reservationModelAttribute");
//		// On déclare un objet de type Vol correspondant au vol selectionnée
//	    Vol vol =  volService.recupererVol(idVol);
//		// On place dans l'objet mav tous les passagers
//		mav.addObject("passagers", passagerService.recupererPassagers());
//		// On place dans l'objet mav tous les services
//		mav.addObject("services", serviceService.recupererServices());
//		// On crée un objet reservation et on l'associe au vol selectionné
//		Reservation reservation = new Reservation();
//		reservation.setVol(vol);
//		// NB: l'objet reservation va correspondre sur la JSP au modelAttribute
//		mav.addObject("reservation", reservation);
//		return mav;
//	}
	
	//on demande a spring de valider l'objet avec @Valid
//	@PostMapping(value= {"/reservationModelAttribute"})
//	public ModelAndView reservationModelAttributePost(@Valid @ModelAttribute("reservation") Reservation reservation, BindingResult result) {
//		// on demande a spring si la validation de l'objet a généré des erreurs 
//		if (result.hasErrors()) {
//			//on recupere l'ID du vol en champs caché (Hidden => dans la jsp). 
//			Vol vol = volService.recupererVol(reservation.getVol().getId());
//			reservation.setVol(vol);
//			// on invoque la methode reservationModelAttributeGet, le mav qu'elle renvoie nous sert de base 
//			// pour renvoyer un mav. 
//			ModelAndView mav = reservationModelAttributeGet(reservation.getVol().getId());
//			// on place de nouveau l'objet reservation dans le mav
//			mav.addObject("reservation", reservation); 
//			return mav;
//			} else {
//				// tout est valid on peut enregistrer l'objet reservation en base
//				// pour ce faire on appel la methode ajouter() de l'objet reservation
//				reservation.setDateHeureReservation(new Date());
//				ModelAndView mav = new ModelAndView("confirmationReservation");
//				reservationService.ajouterReservation(reservation);
//				
//				return mav; 
//			}
//					 
//	}
	
//	@InitBinder
//	public void initBinder(WebDataBinder binder) {
//		binder.registerCustomEditor(Passager.class, "passager", new PropertyEditorSupport() {
//			@Override
//			public void setAsText(String id) {
//			setValue((id.contentEquals("")) ? null : passagerService.recupererPassager(Long.parseLong((String) id)));
//			}
//		});
//		
//		// Apprend à Spring à convertir une liste d’id en liste de services
//		binder.registerCustomEditor(List.class, "services", new CustomCollectionEditor(List.class) {
//		@Override
//		public Object convertElement(Object service) { 
//			Long id = Long.parseLong((String) service); return serviceService.recupererService(id);
//		} });
//	}
//	
//	
//	@PostMapping(value= {"/reservation"})
//	public ModelAndView reservationPost(@RequestParam(name="ID_VOL") Long idVol,
//			@RequestParam(name="ID_PASSAGER") Long idPassager,
//			@RequestParam(name="CRYPTOGRAMME") String cryptogramme,
//			@RequestParam(name="ANNEE_EXPIRATION") Integer anneeExpiration,
//			@RequestParam(name="MOIS_EXPIRATION") Integer moisExpiration,
//			@RequestParam(name="NUMERO_CARTE") String numeroCarte,
//			@RequestParam(name="ID_SERVICE") List<Long> idServicesChoisis) {
//		Reservation reservation = new Reservation();
//		reservation.setDateHeureReservation(new Date());
//		reservation.setCryptogramme(cryptogramme);
//		reservation.setAnneeExpiration(anneeExpiration);
//		reservation.setMoisExpiration(moisExpiration);
//		reservation.setNumeroCarte(numeroCarte);
//		reservation.setPassager(passagerService.recupererPassager(idPassager));
//		reservation.setVol(volService.recupererVol(idVol));
//		for (Long idService : idServicesChoisis) {
//			reservation.getServices().add(serviceService.recupererService(idService));
//		}
//		reservation = reservationService.ajouterReservation(reservation);
//		ModelAndView mav = new ModelAndView("confirmationReservation");
//		mav.addObject("reservation", reservation);
//		return mav;
//
//	}
//	
	// PostConstruct: annotation qui permet de demander à Spring
	// d'exécuter la méthode quand Spring a créé tous les objets du framework
	@PostConstruct
	public void init() {
		System.out.println("Dans la méthode init...");
		Random random = new Random(); 
		
		// On ajoute les aéroports
		if (aeroportService.recupererAeroports().isEmpty()) {
			aeroportService.ajouterAeroport("Saint-Exupery");
			aeroportService.ajouterAeroport("JFK");
			aeroportService.ajouterAeroport("LAX");
			aeroportService.ajouterAeroport("Charles de Gaulle");
			aeroportService.ajouterAeroport("Orly");
			aeroportService.ajouterAeroport("Pierre-Elliott-Trudeau");
			aeroportService.ajouterAeroport("Heathrow");
			aeroportService.ajouterAeroport("Adolfo-Suárez");
			aeroportService.ajouterAeroport("Kingsford-Smith");
		}
		
		if (dureeService.recupererDurees().isEmpty()) {
			dureeService.ajouterDuree(60, aeroportService.recupererAeroportParNom("Saint-Exupery"),
					aeroportService.recupererAeroportParNom("Charles de Gaulle"));
			dureeService.ajouterDuree(180, aeroportService.recupererAeroportParNom("Heathrow"),
					aeroportService.recupererAeroportParNom("Kingsford-Smith"));
		}
		
		// On ajoute les passagers
		if (passagerService.recupererPassagers().isEmpty()) {
			passagerService.ajouterPassager("AUBIN", "Mickael","maubin@humanbooster.com","Mickalel00");
			passagerService.ajouterPassager("BELGACEM", "Mouhssine","mbelgacem@humanbooster.com","123456");
			passagerService.ajouterPassager("COTE", "François-Xavier","fcote@humanbooster.com","123456");
			passagerService.ajouterPassager("GHABRID", "Fatma Zohra","fghabrid@humanbooster.com","123456");
			passagerService.ajouterPassager("GHORRI", "Mejdi","mghorri@humanbooster.com","123456");
			passagerService.ajouterPassager("KELAI", "Soukaina","skelai@humanbooster.com","123456");
			passagerService.ajouterPassager("LONGY", "Guillaume","glongy@humanbooster.com","123456");
			passagerService.ajouterPassager("MATAC", "Ioana","imatac@humanbooster.com","123456");
			passagerService.ajouterPassager("MAYEMBA", "Eric","emayemba@humanbooster.com","123456");
			passagerService.ajouterPassager("MRINI", "Yacine","ymrini@humanbooster.com","123456");
			passagerService.ajouterPassager("SALEK", "Fadhel","fsalek@humanbooster.com","123456");
		}

		if (typeAppareilService.recupererTypeAppareils().isEmpty()) {
			typeAppareilService.ajouterTypeAppareil("Airbus A318", 158);
			typeAppareilService.ajouterTypeAppareil("Airbus A319", 256);
			typeAppareilService.ajouterTypeAppareil("Airbus A320", 280);
			typeAppareilService.ajouterTypeAppareil("Airbus A321", 316);
			typeAppareilService.ajouterTypeAppareil("Airbus A330", 324);
			typeAppareilService.ajouterTypeAppareil("Airbus A380", 516);
		}

		if (avionService.recupererAvions().isEmpty()) {
			Calendar cal = Calendar.getInstance();
			cal.set(2011, 1, 1);
			avionService.ajouterAvion("F-GUGA", cal.getTime(), typeAppareilService.recupererTypeAppareils().get(0));
			cal.set(2012, 2, 2);
			avionService.ajouterAvion("F-GRHB", cal.getTime(), typeAppareilService.recupererTypeAppareils().get(1));
			cal.set(2013, 3, 3);
			avionService.ajouterAvion("F-GKXC", cal.getTime(), typeAppareilService.recupererTypeAppareils().get(2));
			cal.set(2014, 4, 4);
			avionService.ajouterAvion("F-GMZA", cal.getTime(), typeAppareilService.recupererTypeAppareils().get(3));
			cal.set(2014, 5, 5);
			avionService.ajouterAvion("F-GZCA", cal.getTime(), typeAppareilService.recupererTypeAppareils().get(4));
			cal.set(2014, 6, 6);
			avionService.ajouterAvion("F-HPJA", cal.getTime(), typeAppareilService.recupererTypeAppareils().get(5));
		}
		
		if (serviceService.recupererServices().isEmpty()) {
			serviceService.ajouterService("massage des cervicales", 45.0f);
			serviceService.ajouterService("kiwi", 1.45f);
			serviceService.ajouterService("poulet roti fermier bio", 19.99f);
			serviceService.ajouterService("bière Happy Spring", 6.99f);
			serviceService.ajouterService("bobun et mojito", 29.99f);
		}

		// On ajoute les 56 vols
		if (volService.recupererVols().isEmpty()) {
			Calendar calendrier = Calendar.getInstance();
			calendrier.set(Calendar.HOUR, 6);
			int nbAvions = avionService.recupererAvions().size();
			
			// Opération coûteuse
			//int nbAerodromes = aeroportService.recupererAerodromes().size();
			
			long nbAeroports = aeroportService.recupererNbAeroports();
			calendrier.set(2019, 4, 24, 6, 0, 0);
			int counter = 0;

			for (int i = 0; i < 56; i++) {
				
				calendrier.setTime(calendrier.getTime());
				Date dateHeureDepart = calendrier.getTime();
				
				Aeroport aeroportDepart = aeroportService.recupererAeroports().get(random.nextInt((int) nbAeroports));
				Aeroport aeroportArrivee = aeroportService.recupererAeroports().get(random.nextInt((int) nbAeroports));

				// TODO recuperer la duree de vol entre aerodromeDepart
				// et aerodromeArrivee via le service dureeService
				Calendar calendrier2 = Calendar.getInstance();
				calendrier2.setTime(calendrier.getTime());
				calendrier2.add(Calendar.MINUTE, 25+random.nextInt(100));
				Date dateHeureArrivee = calendrier2.getTime();

				float prixEnEuros = random.nextFloat() * (250f - 49f) + 49f;

								
				Avion avion = avionService.recupererAvions().get(random.nextInt(nbAvions));

				volService.ajouterVol(dateHeureDepart, dateHeureArrivee, prixEnEuros, aeroportDepart, aeroportArrivee,
						avion);
				
				calendrier.add(Calendar.HOUR, 1);
				counter++;
				
				// On passe au jour suivant 
				if (counter == 8) {
					counter = 0;
					// L'heure passe à 6 heures du matin
					calendrier.set(Calendar.HOUR, 6);
					calendrier.add(Calendar.DAY_OF_MONTH, 1);
				}
			}
		}
		
		// On ajoute les réservations en parcourant tous les vols
		if (reservationService.recupererReservations().isEmpty()) {
			for (Vol vol : volService.recupererVols()) {
				// On ajoute 10 réservations
				for (int i=0; i<10; i++) {
					// On choisit un passager au hasard
					Passager passager = passagerService.recupererPassagers().get(random.nextInt(passagerService.recupererPassagers().size()));
					// http://www.validcreditcardnumber.com/
					String numeroCarte = "371449635398431";
					reservationService.ajouterReservation(new Date(), numeroCarte, 12, 2019, "200", passager, vol);
				}
			}
		}

		// Affiche les types d'appareil et pour chaque type d'appareil ses avions
		System.out.println(typeAppareilService.recupererTypeAppareils());
	}
	
}