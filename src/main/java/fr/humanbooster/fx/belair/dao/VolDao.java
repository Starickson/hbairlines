package fr.humanbooster.fx.belair.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.fx.belair.business.Vol;

public interface VolDao extends JpaRepository<Vol, Long> {

}
