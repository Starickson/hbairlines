package fr.humanbooster.fx.belair.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.fx.belair.business.TypeAppareil;

public interface TypeAppareilDao extends JpaRepository<TypeAppareil, Long> {

}
