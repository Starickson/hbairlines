package fr.humanbooster.fx.belair.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.fx.belair.business.Service;

public interface ServiceDao extends JpaRepository<Service, Long> {

}
