package fr.humanbooster.fx.belair.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.fx.belair.business.Passager;

// T: classe métier
// ID: Type d'id de la classe métier
public interface PassagerDao extends JpaRepository<Passager, Long> {

	// Spring va écrire pour nous une classe DAO qui implémente l'interface PassagerDao
	
	Passager findByNomAndPrenom(String nom, String prenom);
}
