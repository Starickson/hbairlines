package fr.humanbooster.fx.belair.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.fx.belair.business.Avion;

public interface AvionDao extends JpaRepository<Avion, Long> {

}
