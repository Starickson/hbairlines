package fr.humanbooster.fx.belair.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.fx.belair.business.Duree;

public interface DureeDao extends JpaRepository<Duree, Long> {

}
