package fr.humanbooster.fx.belair.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.fx.belair.business.Reservation;

public interface ReservationDao extends JpaRepository<Reservation, Long> {

}
