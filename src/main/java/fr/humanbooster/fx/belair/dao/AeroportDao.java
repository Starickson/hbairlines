package fr.humanbooster.fx.belair.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.fx.belair.business.Aeroport;

public interface AeroportDao extends JpaRepository<Aeroport, Long> {

	Aeroport findAeroportByNom(String nom);
}
