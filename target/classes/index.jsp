<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Hb Airways</title>
<link href="<c:url value="/bootstrap/css/my_style.css"/>"
	rel="stylesheet">
<link href="<c:url value="/bootstrap/css/bootstrap.min.css"/>"
	rel="stylesheet">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">HB Airways</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link" href="#">Home
						<span class="sr-only">(current)</span>
				</a></li>
				<li class="nav-item"><a class="nav-link" href="#">Destination</a>
				</li>
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> Countries </a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="#">Europe</a> <a
							class="dropdown-item" href="#">North America</a> <a
							class="dropdown-item" href="#">Sud America</a> <a
							class="dropdown-item" href="#">Asia</a>
					</div></li>
			</ul>
			<form class="form-inline my-2 my-lg-0">
				<a class="btn btn-primary" href="connexion" role="buttonConnexion">Connexion</a>
				<a class="btn btn-primary" href="inscription" role="buttonInscription">Inscription</a>
<!-- 				<input class="form-control mr-sm-2" type="search" -->
<!-- 					placeholder="Search" aria-label="Search"> -->
<!-- 				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> -->
			</form>
		</div>
	</nav>
	<section id="main-image" class="container-fluid">
		<div class="row">
			<div class="col-12">
				<h2>
					Organisez votre<br>
					<strong>voyage sur mesure</strong>
				</h2>
				<a href="listeDesVols" class="button-1">Par ici</a>
			</div>
		</div>
	</section>
	<section id="steps" class="container text-center">
		<div class="row mt-5">
			<div class="col-md-4" id="step-1">
				<img src="/images/steps-icon-1.png" alt="image">
				<h4>Planifier</h4>
				<p>Confiez-nous vos rêves d’évasion : en famille ou entre amis,
					nous trouverons la formule qui comblera vos attentes.</p>
			</div>
			<div class="col-md-4" id="step-2">
				<img src="/images/steps-icon-2.png" alt="image">
				<h4>Organiser</h4>
				<p>Bénéficiez de l’expertise de nos spécialistes de chaque
					destination, ils vous accompagnent dans la réalisation de votre
					voyage.</p>
			</div>
			<div class="col-md-4" id="step-3">
				<img src="/images/steps-icon-3.png" alt="image">
				<h4>Voyager</h4>
				<p>Nous nous chargeons d’assurer votre sécurité et de veiller à
					votre pleine sérénité tout au long de votre voyage.</p>
			</div>
		</div>
	</section>
	<section class="container-fluid" id="possibilities">
		<div class="row">
			<div class="col-md-6 d-flex justify-content-end">
				<article style="background-image: url(images/article-image-1.jpg);">
					<div class="overlay">
						<h4>Partez en famille</h4>
						<p>
							<small>Offrez le meilleur à ceux que vous aimez et
								partagez des moments fabuleux !</small>
						</p>
						<a href="#" class="button-2">Plus d'infos</a>
					</div>
				</article>
			</div>

			<div class="col-md-6">
				<article style="background-image: url(images/article-image-2.jpg);">
					<div class="overlay">
						<h4>Envie de s'evader</h4>
						<p>
							<small>Parfois un peu d'évasion serait le bienvenue et
								ferait le plus grand bien !</small>
						</p>
						<a href="#" class="button-2">Plus d'infos</a>
					</div>
				</article>
			</div>
			<div class="clear"></div>

		</div>
	</section>
	<section class="container" id="contact">
		<div class="row">
			<div class="col-md-12">
				<h3>Contactez-nous</h3>
				<p>Chez Travel Agency nous savons que voyager est une aventure
					humaine mais également un engagement financier important pour vous.
					C'est pourquoi nous mettons un point d'honneur à prendre en compte
					chacune de vos attentes pour vous aider dans la préparation de
					votre séjour, circuit ou voyage sur mesure.</p>

				<form>
					<label for="name">Nom</label> <input type="text" id="name"
						placeholder="Votre nom"> <label for="email">Email</label>
					<input type="text" id="email" placeholder="Votre email"> <input
						type="submit" value="OK" class="button-3">
				</form>
			</div>

		</div>
	</section>
</body>
</html>