<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Réservation</title>
<link href="<c:url value="/bootstrap/css/my_style.css"/>"
	rel="stylesheet">
<link href="<c:url value="/bootstrap/css/bootstrap.min.css"/>"
	rel="stylesheet">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">HB Airways</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link" href="#">Home
						<span class="sr-only">(current)</span>
				</a></li>
				<li class="nav-item"><a class="nav-link" href="#">Destination</a>
				</li>
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> Countries </a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="#">Europe</a> <a
							class="dropdown-item" href="#">North America</a> <a
							class="dropdown-item" href="#">Sud America</a> <a
							class="dropdown-item" href="#">Asia</a>
					</div></li>
			</ul>
			<form class="form-inline my-2 my-lg-0">
				<a class="btn btn-primary" href="connexion" role="buttonConnexion">Connexion</a>
				<a class="btn btn-primary" href="inscription" role="buttonInscription">Inscription</a>
<!-- 				<input class="form-control mr-sm-2" type="search" -->
<!-- 					placeholder="Search" aria-label="Search"> -->
<!-- 				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> -->
			</form>
		</div>
	</nav>
<form action="reservation" method="post">
<label>Vol : </label>De ${vol.aeroportDepart.nom} à ${vol.aeroportArrivee.nom}
<input type="hidden" name="ID_VOL" value="${vol.id}"><br>
<label>&nbsp;</label>Départ le ${vol.dateHeureDepartFormatee}<br>
<label>&nbsp;</label>Arrivée le ${vol.dateHeureArriveeFormatee}<br>
<br>
<label>Avion :</label> ${vol.avion.immatriculation} de type ${vol.avion.typeAppareil.nom}<br>
<br>
<label>Prix</label>${vol.prixEnEuros} €<br><br>
<label>Passager</label><select name="ID_PASSAGER">
<c:forEach items="${passagers}" var="passager">
<option value="${passager.id}">${passager.prenom} ${passager.nom}</option>
</c:forEach>
</select>
<br><br>
<label>Service(s) souhaité(s)</label>
<select name="ID_SERVICE" multiple="multiple" size="6">
<c:forEach items="${services}" var="service">
<option value="${service.id}">${service.nomEtPrixEnEuros}</option>
</c:forEach>
</select>
<br>
<label>Numéro de carte</label><input type="text" name="NUMERO_CARTE" maxlength="16"><br>
<label>Mois expiration</label><input type="text" name="MOIS_EXPIRATION"/><br>
<label>Année expiration</label><input type="text" name="ANNEE_EXPIRATION"/><br>
<label>Cryptogramme</label><input type="text" name="CRYPTOGRAMME" maxlength="3"/><br><br>
<label>&nbsp;</label><input type="submit" value="Réserver">
</form>
</body>
</html>